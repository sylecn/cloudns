PYTHON_MODULES = cloudns
PYTHONPATH = .
PYTHON = env PYTHONPATH=$(PYTHONPATH) bin/python
PYTEST = env PYTHONPATH=$(PYTHONPATH) PYTEST=1 bin/py.test --durations=3
PYLINT = env PYTHONPATH=$(PYTHONPATH) bin/pylint --msg-template='{path}:{line}: [{msg_id}({symbol}), {obj}] {msg} % (self.name, self.line_format))--msg-template={path}:{line}: [{msg_id}({symbol}), {obj}] {msg} % (self.name, self.line_format))' -d I0011,R0913,W0622,R0201,W0201,R0904

build: sdist
sdist: clean
	$(PYTHON) setup.py sdist
upload:
	$(PYTHON) setup.py sdist upload
clean:
	rm -rf build/ dist/ cloudns.egg-info/
clean-all: clean
	rm -rf local/ lib/ bin/ include/ build/ dist/ cloudns.egg-info/
	find . -name "__pycache__" -type d -exec rm -rf {} \; || true
	find . -name "*.pyc" -type f -exec rm -rf {} \; || true
cleanpy:
	rm -rf lib/python*/site-packages/cloudns-*
bootstrap:
	@echo "bootstrapping, this will take a while..."
	@./utils/bootstrap all
check: test
test: cleanpy bootstrap tox-test
tox-test:
	$(PYLINT) -E $(PYTHON_MODULES)
	$(PYTEST) $(PYTHON_MODULES)
pylint:
	$(PYLINT) $(PYTHON_MODULES)
shell:
	$(PYTHON) -i -c 'import cloudns'
doc:
	PYTHONPATH=. make html -C doc/
TAGS:
	etags -R $(PYTHON_MODULES)
.PHONY: upload clean clean-all test check run bootstrap TAGS shell sdist doc
