cloudns command line program
============================

Cloudns command line program is available in v1.1.1.0+. For quick start, see
https://pypi.python.org/pypi/cloudns

Authentication
--------------

cloudns requires a passport and a token for user authentication. To apply for
an account, please contact YY-game department.

There are a few ways to specify authentication information. The earlier ones
has higher priority, thus overwrite the later ones.

  - command line arguments --passport --token
  - options defined in config file that is specified by -f
  - when -f is not used, options defined in default config file, which is
    $HOME/.config/cloudns.conf

Config File Format
------------------

You can set passport and token in config file using A=B syntax. Other lines
are ignored. Note that config variable names must be upper case. The default
config file is $HOME/.config/cloudns.conf, you can overwrite this using the -f
option.

.. code-block:: sh

   # config file for cloudns shell program
   PASSPORT=my_passport
   TOKEN=my_token

Subcommand Reference
--------------------
Here is a reference of all subcommands available.

Direct invocation
^^^^^^^^^^^^^^^^^

Check ``cloudns --help`` for options. These subcommands should be used after
all options.

 .. function:: create label content tel|uni [type [ttl]]

    Create a new record in DNS system.

    :label: DNS label
    :content: DNS value
    :isp: tel for China Telcom, uni for China Unicom
    :type: record type, default is A, could be CNAME
    :ttl: DNS ttl in seconds, default is 300

 .. function:: search keyword

    Search existing record by keyword in label. if * is not used, search label
    that contains the keyword. Otherwise, do shell glob like matching.

    :keyword: a keyword in lable. You can use * to do fuzzy matching. for
              example v*z

 .. function:: delete label

    Delete existing records that match exactly the given label.

    :lable: the exact record label to delete


Interactive shell
^^^^^^^^^^^^^^^^^

Type ``help`` in interactive shell can get a reference of all commands.
Type ``help <subcommand>`` to get help on that command.

Here is the current list

  * set_passport
  * set_token
  * set_zone
  * info
  * create
  * search
  * delete
  * clear_cache
  * exit/quit

create, search, delete has similar syntax and exactly the same semantics as
the direct invoke subcommands.

You can also exit the shell by pressing Ctrl-d.
