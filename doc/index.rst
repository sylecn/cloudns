.. cloudns documentation master file, created by
   sphinx-quickstart on Wed Aug 14 15:05:17 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to cloudns's documentation!
===================================

cloudns module is a python client for the YY cloudns API.

You can read about YY cloudns API at
http://www.nsbeta.info/doc/YY-DNS-API.pdf

For installation instructions and quick start see
https://pypi.python.org/pypi/cloudns

For detailed API document, check modules below. The key module is user and
zone.

.. toctree::
   :maxdepth: 3

   modules/modules
   cloudns_cli

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

